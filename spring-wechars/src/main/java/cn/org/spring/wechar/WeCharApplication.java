package cn.org.spring.wechar;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 微信公众平台启动类
 *
 * @auther: wanghongjie
 * @blame: wanghongjie
 * @date: 2020-10-08 17:18
 * @Description:
 */
@SpringBootApplication
public class WeCharApplication {
    public static void main(String[] args) {
        SpringApplication.run(WeCharApplication.class);
    }
}
