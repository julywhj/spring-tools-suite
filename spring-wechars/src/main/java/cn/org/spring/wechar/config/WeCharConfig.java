package cn.org.spring.wechar.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @auther: wanghongjie
 * @blame: wanghongjie
 * @date: 2020-10-11 10:37
 * @Description:
 */
@Component
@Data
@ConfigurationProperties(prefix = "we-char")
public class WeCharConfig {
    /**
     * 用户唯一标识
     */
    private String appId;
    /**
     * 用户密钥
     */
    private String secret;
    /**
     * 加密token
     */
    private String token;

    /**
     * 监听队列
     */
    private String weCharQueue;
    /**
     * 交换机
     */
    private String weCharExchange;
    /**
     * 绑定的标示
     */
    private String directKey;
}
