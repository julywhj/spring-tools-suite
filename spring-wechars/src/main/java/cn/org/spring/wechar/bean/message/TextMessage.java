package cn.org.spring.wechar.bean.message;

import cn.org.spring.common.util.XmlUtils;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User:  wanghongjie
 * Date:  2020/10/17 - 10:01
 * <p>
 * Description:
 */
@Data
@XStreamAlias("xml")
public class TextMessage extends BaseMessage {

    @XStreamAlias("Content")
    private String content;

    public static TextMessage of(Map<String, Object> objectMap, String content) {
        TextMessage textMessage = new TextMessage();
        textMessage.init(objectMap);
        textMessage.setContent(content);
        return textMessage;
    }

    public static TextMessage ofSendMsg(Map<String, Object> objectMap, String content) {
        TextMessage textMessage = new TextMessage();
        textMessage.init(objectMap);
        textMessage.setContent(content);
        textMessage.setMsgType("text");
        String from = textMessage.getFromUserName();
        textMessage.setFromUserName(textMessage.getToUserName());
        textMessage.setToUserName(from);
        return textMessage;
    }

    public String toXml() {
        return XmlUtils.beanToXml(this, TextMessage.class);
    }
}