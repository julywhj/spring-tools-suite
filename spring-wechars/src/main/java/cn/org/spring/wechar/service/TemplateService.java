package cn.org.spring.wechar.service;

import cn.org.spring.common.util.HttpClientUtils;
import cn.org.spring.wechar.constant.WeCharConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * 模版消息处理服务类
 * Created with IntelliJ IDEA.
 * User:  wanghongjie
 * Date:  2020/10/29 - 19:40
 * <p>
 * Description:
 */
@Service
public class TemplateService {

    @Autowired
    private AccessTokenService accessTokenService;

    /**
     * 设置行业信息
     *
     * @param body 请求对象
     * @return
     * @throws IOException
     */
    public String setIndustry(String body) throws IOException {
        return HttpClientUtils.post(WeCharConstant.SET_INDUSTRY.replace("ACCESS_TOKEN", accessTokenService.getAccessToken()),
                body);
    }

    /**
     * 获取行业信息
     *
     * @return
     * @throws IOException
     */
    public String getIndustry() throws IOException {
        return HttpClientUtils.get(WeCharConstant.GET_INDUSTRY.replace("ACCESS_TOKEN", accessTokenService.getAccessToken()));
    }

    /**
     * 获取微信公众平台全部模版消息
     *
     * @return
     * @throws IOException
     */
    public String getTemplateList() throws IOException {
        return HttpClientUtils.get(WeCharConstant.GET_ALL_PRIVATE_TEMPLATE.replace("ACCESS_TOKEN", accessTokenService.getAccessToken()));
    }

    /**
     * 发送模版消息
     *
     * @param body 请求数据
     * @return
     * @throws IOException
     */
    public String send(String body) throws IOException {
        return HttpClientUtils.post(WeCharConstant.TEMPLATE_SEND.replace("ACCESS_TOKEN", accessTokenService.getAccessToken()), body);
    }
}