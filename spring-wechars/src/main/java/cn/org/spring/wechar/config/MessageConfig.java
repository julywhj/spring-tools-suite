package cn.org.spring.wechar.config;

import cn.org.spring.wechar.constant.MessageType;
import cn.org.spring.wechar.context.ContextMessageFactory;
import cn.org.spring.wechar.service.message.EventMessageService;
import cn.org.spring.wechar.service.message.ImageMessageService;
import cn.org.spring.wechar.service.message.NewsMessageService;
import cn.org.spring.wechar.service.message.TextMessageService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 消息配置类
 * Created with IntelliJ IDEA.
 * User:  wanghongjie
 * Date:  2020/10/18 - 19:50
 * <p>
 * Description:
 */
@Configuration
public class MessageConfig {

    private ApplicationContext applicationContext;

    public MessageConfig(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Bean
    public ContextMessageFactory getContextMessageFactory() {
        ContextMessageFactory contextMessageFactory = new ContextMessageFactory();
        contextMessageFactory.builder(MessageType.IMAGE, applicationContext.getBean(ImageMessageService.class))
                .builder(MessageType.TEXT, applicationContext.getBean(TextMessageService.class))
                .builder(MessageType.LINK, applicationContext.getBean(NewsMessageService.class))
                .builder(MessageType.EVENT, applicationContext.getBean(EventMessageService.class));
        return contextMessageFactory;
    }

}