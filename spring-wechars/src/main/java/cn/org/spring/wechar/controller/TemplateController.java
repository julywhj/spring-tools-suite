package cn.org.spring.wechar.controller;

import cn.org.spring.wechar.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User:  wanghongjie
 * Date:  2020/10/29 - 19:51
 * <p>
 * Description:
 */
@RestController
@RequestMapping("/v1/weChar")
public class TemplateController {

    @Autowired
    private TemplateService templateService;

    @RequestMapping("/setIndustry")
    public String setIndustry(@RequestBody String body) throws IOException {
        return templateService.setIndustry(body);
    }

    @RequestMapping("/getIndustry")
    public String setIndustry() throws IOException {
        return templateService.getIndustry();
    }

    @RequestMapping("/getTemplateList")
    public String getTemplateList() throws IOException {
        return templateService.getTemplateList();
    }

    @RequestMapping("/send")
    public String send(@RequestBody String body) throws IOException {
        return templateService.send(body);
    }

}