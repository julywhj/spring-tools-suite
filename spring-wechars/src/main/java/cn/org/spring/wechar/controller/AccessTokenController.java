package cn.org.spring.wechar.controller;

import cn.org.spring.wechar.service.AccessTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @auther: wanghongjie
 * @blame: wanghongjie
 * @date: 2020-10-11 10:56
 * @Description:
 */
@RestController
@RequestMapping("/accessToken")
public class AccessTokenController {
    @Autowired
    private AccessTokenService accessTokenService;

    @RequestMapping
    public String getAccessToken() throws IOException {
        return accessTokenService.getAccessToken();
    }
}
