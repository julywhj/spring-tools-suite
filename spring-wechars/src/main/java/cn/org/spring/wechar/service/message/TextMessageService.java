package cn.org.spring.wechar.service.message;

import cn.org.spring.wechar.bean.message.TextMessage;
import cn.org.spring.wechar.config.WeCharConfig;
import cn.org.spring.wechar.constant.WeCharConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User:  wanghongjie
 * Date:  2020/10/18 - 19:29
 * <p>
 * Description:
 */
@Service
@Slf4j
public class TextMessageService implements IMessage {

    private final WeCharConfig weCharConfig;

    public TextMessageService(WeCharConfig weCharConfig) {
        this.weCharConfig = weCharConfig;
    }


    @Override
    public String handler(Map<String, Object> stringObjectMap) {
        String content = "嗨咯你好呀!";
        if (stringObjectMap.get(WeCharConstant.CONTENT).toString().equals("登录")) {
            content = WeCharConstant.OAUTH2_AUTHORIZE.replace("APPID", weCharConfig.getAppId())
                    .replace("REDIRECT_URI", "http://192.168.31.123:8080/v1/weChar/getCode").replace("SCOPE", "snsapi_userinfo");
            System.out.println(content);
            content = "您好，请点击<a href='" + content + "'>登录</a>进行更多的操作。";
        }
        TextMessage textMessage = TextMessage.ofSendMsg(stringObjectMap, content);
        return textMessage.toXml();
    }

}