package cn.org.spring.wechar.controller;

import cn.org.spring.common.util.HttpClientUtils;
import cn.org.spring.wechar.bean.Button;
import cn.org.spring.wechar.bean.ButtonType;
import cn.org.spring.wechar.constant.WeCharConstant;
import cn.org.spring.wechar.service.AccessTokenService;
import cn.org.spring.wechar.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User:  wanghongjie
 * Date:  2020/10/14 - 21:13
 * <p>
 * Description:
 */
@RestController
@RequestMapping("/v1/weChar")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @Resource
    private AccessTokenService accessTokenService;

    @GetMapping("/createMenu")
    public void createMenu() throws IOException {
        Button one_1 = Button.ofOneMenu(Button.HEAD, ButtonType.VIEW, "关于我们", "http://hryreadingroom.cn/about/");
        Button one_2 = Button.ofOneMenu(one_1, ButtonType.CLICK, "活动", "尚无活动");
        Button one_3 = Button.ofOneMenu(one_2, ButtonType.CLICK, "会员中心", "会员中心");
        Button of = Button.of(one_3, 2, ButtonType.VIEW, "我的会员", "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf27d27eb0e8e398c&redirect_uri=http://192.168.31.111:8081/mobile/v1/user/oauthLogin/&response_type=code&scope=snsapi_userinfo#wechat_redirect");
        Button of1 = Button.of(of, 2, ButtonType.VIEW, "周边商户", "http://192.168.31.111:8081/mobile/v1/artist");
        Button of2 = Button.of(of1, 2, ButtonType.VIEW, "积分商城", "http://192.168.31.111:8081/mobile/v1/goods");
        Button of3 = Button.of(of2, 2, ButtonType.VIEW, "切换用户", "http://192.168.31.111:8081/mobile/v1/user/logout");
        String url = WeCharConstant.CREATE_MENU_URL.replace("ACCESS_TOKEN", accessTokenService.getAccessToken());
        System.out.println(of3.toJson());
        String post = HttpClientUtils.post(url, of3.toJson());
        System.out.println(post);
    }

    @GetMapping("/deleteMenu")
    public void deleteMenu() throws IOException {
        menuService.deleteMenu();
    }


}