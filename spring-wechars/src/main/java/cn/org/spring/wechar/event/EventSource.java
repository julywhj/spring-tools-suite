package cn.org.spring.wechar.event;

/**
 * 事件源接口
 * Created by wanghongjie on 2020/10/25 10:51
 */
public interface EventSource {
    /**
     * 增加事件监听器
     *
     * @param eventListener 事件监听
     */
    void addListener(EventListener eventListener);

    /**
     * 通知事件监听器
     */
    void notifyListener();
}
